Smooth Drop
===========

A simple snippet of CSS for adding *smooth* "cubic-bezier" animation to Bootstrap dropdown menus.
